Hologram [![License](https://img.shields.io/badge/License-MIT-lightgrey.svg?style=flat)](http://mit-license.org) 
==========

Hologram is a simple shader made in Unity.
I've been building over the [original](https://github.com/andydbc/HologramShader) with various changes and tweaks. 