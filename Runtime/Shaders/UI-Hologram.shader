﻿// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)
// Quick modification of the Unity builtin UI shader to fix MSAA falling out of bounds on interpolating text quads - Merlin
// Added hologram effects - Silent

Shader "UI/SF Hologram Shader UI"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        [HDR]_Color ("Tint", Color) = (1,1,1,1)

        [Header(Scanline)]
        [ToggleUI]_UseScan ("Use Scanline", Float) = 0.0
        _ScanTiling ("Scan Tiling", Range(0.01, 100.0)) = 0.05
        _ScanSpeed ("Scan Speed", Range(-2.0, 2.0)) = 1.0
        [Header(Glow)]
        [ToggleUI]_UseGlow ("Use Glow", Float) = 0.0
        _GlowTiling ("Glow Tiling", Range(0.01, 1.0)) = 0.05
        _GlowSpeed ("Glow Speed", Range(-10.0, 10.0)) = 1.0
        [Header(Glitch)]
        [ToggleUI]_UseGlitch ("Use Glitch", Float) = 0.0
        _GlitchSpeed ("Glitch Speed", Range(0, 50)) = 1.0
        _GlitchIntensity ("Glitch Intensity", Float) = 0
        [Header(Alpha Flicker)]
        _FlickerTex ("Flicker Control Texture", 2D) = "white" {}
        _FlickerSpeed ("Flicker Speed", Range(0.01, 100)) = 1.0

        [Header(Stencil)]
        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
        [Toggle(_NO_SSAA)] _DisableAntialiasing ("Disable local anti-aliasing", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
            Pass [_StencilOp]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }

        Cull Off
        Lighting Off
        ZWrite Off
        ZTest [unity_GUIZTestMode]
        Blend SrcAlpha OneMinusSrcAlpha
        ColorMask [_ColorMask]
		BlendOp Add, Max

        Pass
        {
            Name "Default"
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            #include "UnityCG.cginc"
            #include "UnityUI.cginc"
            #include "HologramCommon.hlsl"

            #pragma multi_compile __ UNITY_UI_CLIP_RECT
            #pragma multi_compile __ UNITY_UI_ALPHACLIP
            #pragma shader_feature_local __ _NO_SSAA

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                float2 generatedtexcoord : TEXCOORD2;
                centroid float2 centroidtexcoord : TEXCOORD3;
                UNITY_VERTEX_OUTPUT_STEREO
            };

            fixed4 _Color;
            fixed4 _TextureSampleAdd;
            float4 _ClipRect;

            float4 _MainTex_TexelSize;

            uniform sampler2D _FlickerTex;
            uniform float _UseGlitch; 
            uniform float _GlitchSpeed;
            uniform float _GlitchIntensity;
            uniform float _Brightness;
            uniform float _Alpha;
            uniform float _UseScan; 
            uniform float _ScanTiling;
            uniform float _ScanSpeed;
            uniform float _UseGlow;
            uniform float _GlowTiling;
            uniform float _GlowSpeed;
            uniform float _FlickerSpeed;

            #define USE_RGSSAA (!defined(_NO_SSAA))

            v2f vert(appdata_t v, uint vertID : SV_VertexID)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                //v.vertex = UnityPixelSnap (v.vertex);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);
                // Glitches
                v.vertex.x += _UseGlitch * ApplyGlitch(v.vertex.y, _GlitchIntensity, _GlitchSpeed, _Time.y);

                OUT.texcoord = v.texcoord;

                OUT.color = v.color * _Color;
                OUT.generatedtexcoord = float2(0, 0);
                OUT.centroidtexcoord = v.texcoord;

                // This can be done better, but I'm too lazy at the moment...
                switch (vertID % 4)
                {
                case 0:
                    OUT.generatedtexcoord = float2(0, 0);
                    break;
                case 1:
                    OUT.generatedtexcoord = float2(1, 0);
                    break;
                case 2:
                    OUT.generatedtexcoord = float2(1, 1);
                    break;
                case 3:
                    OUT.generatedtexcoord = float2(0, 1);
                    break;
                }

                return OUT;
            }

            sampler2D _MainTex;

            half4 frag(v2f IN) : SV_Target
            {
                float2 texcoord = IN.texcoord;

                // Use Valve method of falling back to centroid interpolation if you fall out of valid interpolation range
                // Based on slide 44 of http://media.steampowered.com/apps/valve/2015/Alex_Vlachos_Advanced_VR_Rendering_GDC2015.pdf
                if (any(IN.generatedtexcoord > 1.0) || any(IN.generatedtexcoord < 0.0) )
                    texcoord = IN.centroidtexcoord;

                #if defined(USE_RGSSAA)
				half4 color = 0;
                {
                	float _Bias = 0;
                	texcoord = sharpSample2(_MainTex_TexelSize, texcoord);
	                // per pixel partial derivatives
					float2 dx = ddx(IN.texcoord);
					float2 dy = ddy(IN.texcoord);// manually calculate the per axis mip level, clamp to 0 to 1
					// and use that to scale down the derivatives
					if (0){
					dx *= saturate(
					    0.5 * log2(dot(dx * _MainTex_TexelSize.zw, dx * _MainTex_TexelSize.zw))
					    );
					dy *= saturate(
					    0.5 * log2(dot(dy * _MainTex_TexelSize.zw, dy * _MainTex_TexelSize.zw))
					    );// rotated grid uv offsets
					}
					float2 uvOffsets = float2(0.125, 0.375);
					float4 offsetUV = float4(0.0, 0.0, 0.0, _Bias);// supersampled using 2x2 rotated grid
					offsetUV.xy = texcoord + uvOffsets.x * dx + uvOffsets.y * dy;
					color += tex2Dbias(_MainTex, offsetUV);
					offsetUV.xy = texcoord - uvOffsets.x * dx - uvOffsets.y * dy;
					color += tex2Dbias(_MainTex, offsetUV);
					offsetUV.xy = texcoord + uvOffsets.y * dx - uvOffsets.x * dy;
					color += tex2Dbias(_MainTex, offsetUV);
					offsetUV.xy = texcoord - uvOffsets.y * dx + uvOffsets.x * dy;
					color += tex2Dbias(_MainTex, offsetUV);
					color *= 0.25;
					color = (color + _TextureSampleAdd) * IN.color;
                }
                #else
                texcoord = sharpSample2(_MainTex_TexelSize, texcoord);
                half4 color = (tex2D(_MainTex, texcoord) + _TextureSampleAdd) * IN.color;
				#endif

                // Scanlines
				float scan = 0.0;
				scan += _UseScan * ApplyScan(IN.worldPosition.y, _ScanTiling, _ScanSpeed, _Time.w);

                // Glow
                float glow = 0.0;
                glow += _UseGlow * frac(IN.worldPosition.y * _GlowTiling - _Time.x * _GlowSpeed);

                // Flicker
                fixed4 flicker = tex2D(_FlickerTex, _Time * _FlickerSpeed);

                #ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif

                #ifdef UNITY_UI_ALPHACLIP
                clip (color.a - 0.001);
                #endif

                color += color*(glow * 0.35);
                color.a *= (scan + glow + 1) * flicker;

                color.a = saturate(color.a);

                return color;
            }
        ENDCG
        }
    }
}
