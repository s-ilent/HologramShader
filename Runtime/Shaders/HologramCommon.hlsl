#ifndef HOLOGRAM_COMMON
#define HOLOGRAM_COMMON

float ApplyGlitch(float vertexPosition, float glitchIntensity, float glitchSpeed, float time)
{
    float glitch = step(0.5, sin(time * 2.0 + vertexPosition)) * step(0.99, sin(time * glitchSpeed * 0.5));
    glitch += step(0.3, sin(time * 3.0 + vertexPosition)) * step(0.98, sin(time * glitchSpeed * 0.4));
    glitch += step(0.7, sin(time * 1.5 + vertexPosition)) * step(0.95, sin(time * glitchSpeed * 0.6));
    
    return glitch * glitchIntensity;
}

// Returns pixel sharpened to nearest pixel boundary. 
// texSize is Unity _Texture_TexelSize; zw is w/h, xy is 1/wh
float2 sharpSample2( float4 texSize , float2 coord )
{
    float2 boxSize = clamp(fwidth(coord) * texSize.zw, 1e-5, 1);
    coord = coord * texSize.zw - 0.5 * boxSize;
    float2 txOffset = smoothstep(1 - boxSize, 1, frac(coord));
    return(floor(coord) + 0.5 + txOffset) * texSize.xy; 
}

float2 getSpriteUVs (float2 uvs, float time, float animationSpeed, float frameNumber, float columns, float rows) {
    // From the frame number, get the row and column of the frame on the sprite sheet.
    float frame_num = time * animationSpeed + frameNumber;
    int2 frame = int2(floor(fmod(frame_num, columns)), floor(fmod((frame_num / columns), rows)));
    
    return float2((uvs.x + frame[0]) / columns, ((uvs.y - frame[1]) / rows) + (rows - 1.0) / rows);
}

half2 getMatcapUVs(float3 normal, float3 viewDir)
{
    // Based on Masataka Sumi's implementation
    half3 worldUp = float3(0, 1, 0);
    half3 worldViewUp = normalize(worldUp - viewDir * dot(viewDir, worldUp));
    half3 worldViewRight = normalize(cross(viewDir, worldViewUp));
    return half2(dot(worldViewRight, normal), dot(worldViewUp, normal)) * 0.5 + 0.5;
}

float filtered_cosine(float2 x, float alpha, float2 f)
{
    float2x2 J = 0.5 * float2x2(ddx(x), ddy(x));
    float2x2 S = mul(J, transpose(J));
    return alpha * cos(dot(x, f)) * exp(-0.5 * dot(f, mul(S, f)));
}

float ApplyScan(float position, float scanTiling, float scanSpeed, float time)
{
    #if 0
    float scanS, scanC; 
    sincos((10 * position * scanTiling + time * scanSpeed), scanS, scanC);
    float scan = abs(scanS * scanC) * 0.65;
    #else
    float2 f = 2.0;
    float scan = filtered_cosine(10 * position * scanTiling + time * scanSpeed, 0.65, f);
    #endif
    
    return scan;
}

#endif // HOLOGRAM_COMMON