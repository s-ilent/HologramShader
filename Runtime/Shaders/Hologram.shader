﻿Shader "Silent/SF Hologram Shader Plus"
{
	Properties
	{
		[Header(General)]
		_Brightness("Brightness", Range(0.1, 6.0)) = 3.0
		_Alpha ("Alpha", Range (0.0, 1.0)) = 1.0
		[Header(Main Color)]
		[ToggleUI]_UseSharp ("Pixel Sampling", Float) = 0.0
		_MainTex ("Main Texture", 2D) = "white" {}
		_MainColor ("Tint", Color) = (1,1,1,1)
		[Header(Add Color)]
		_AddTex ("Add Texture", 2D) = "black" {}
		_AddColor ("AddColor", Color) = (1,1,1,1)
		[Header(Rim)]
		_RimColor ("Rim Color", Color) = (1,1,1,1)
		_RimPower ("Rim Power", Range(0.1, 10)) = 5.0
		[Header(Scanline)]
		[ToggleUI]_UseScan ("Use Scanline", Float) = 0.0
		[Enum(World Pos, 0, Object Pos, 1, UV1, 2, UV2, 3, View, 4)]_ScanPos("Scan Position Source", Float) = 0
		_ScanTiling ("Scan Tiling", Range(0.01, 100.0)) = 0.05
		_ScanSpeed ("Scan Speed", Range(-2.0, 2.0)) = 1.0
		[Header(Glow)]
		[ToggleUI]_UseGlow ("Use Glow", Float) = 0.0
		_GlowTiling ("Glow Tiling", Range(0.01, 1.0)) = 0.05
		_GlowSpeed ("Glow Speed", Range(-10.0, 10.0)) = 1.0
		[Header(Glitch)]
		[ToggleUI]_UseGlitch ("Use Glitch", Float) = 0.0
		_GlitchSpeed ("Glitch Speed", Range(0, 50)) = 1.0
		_GlitchIntensity ("Glitch Intensity", Float) = 0
		[Header(Alpha Flicker)]
		_FlickerTex ("Flicker Control Texture", 2D) = "white" {}
		_FlickerSpeed ("Flicker Speed", Range(0.01, 100)) = 1.0

		[Header(Spritesheet)]
		_Columns("Columns", int) = 1
		_Rows("Rows", int) = 1
		_FrameNumber ("Frame Number", int) = 0
		_AnimationSpeed ("Frames per Second", float) = 0

		// Settings
		[HideInInspector] _Fold("__fld", Float) = 1.0
	}
	SubShader
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		LOD 100
        Cull Back
		BlendOp Add, Max

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
            #include "HologramCommon.hlsl"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float4 uv : TEXCOORD0;
				float4 worldVertex : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
				float3 worldNormal : NORMAL;
			};

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float4 _MainColor;
			uniform float4 _MainTex_TexelSize;
			
			uniform sampler2D _AddTex;
			uniform float4 _AddTex_ST;
			uniform float4 _AddColor;

			uniform sampler2D _FlickerTex;
			uniform float4 _RimColor;
			uniform float _RimPower;

			uniform float _UseGlitch; 
			uniform float _GlitchSpeed;
			uniform float _GlitchIntensity;

			uniform float _Brightness;
			uniform float _Alpha;

			uniform float _UseScan; 
			uniform float _ScanPos;
			uniform float _ScanTiling;
			uniform float _ScanSpeed;

			uniform float _UseGlow;
			uniform float _GlowTiling;
			uniform float _GlowSpeed;
			uniform float _FlickerSpeed;

			uniform int _Columns;
			uniform int _Rows;
			uniform int _FrameNumber;
			uniform float _AnimationSpeed;

			uniform float _UseSharp;
			
			v2f vert (appdata v)
			{
				v2f o;
				
				// Glitches
				v.vertex.x += _UseGlitch * ApplyGlitch(v.vertex.y, _GlitchIntensity, _GlitchSpeed, _Time.y);

				o.vertex = UnityObjectToClipPos(v.vertex);
				
				o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldVertex = mul(unity_ObjectToWorld, v.vertex);
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.viewDir = normalize(UnityWorldSpaceViewDir(o.worldVertex.xyz));
				o.color = v.color;

				float2 matcapUV = getMatcapUVs(o.worldNormal, o.viewDir);

				// Scanline position
				float scanPos[] = { o.worldVertex.y, v.vertex.y, v.uv.y, v.uv2.y, matcapUV.y};
				o.uv.z = scanPos[_ScanPos];
				o.uv.w = 0;

				return o;
			}			

			fixed4 frag (v2f i) : SV_Target
			{
				_MainColor.rgb *= i.color.rgb;
				float2 scrollUVs = getSpriteUVs(i.uv, _Time[1], _AnimationSpeed, _FrameNumber, _Columns, _Rows);
        		float2 sharpUV = sharpSample2(_MainTex_TexelSize, scrollUVs);
        		scrollUVs = _UseSharp? sharpUV : scrollUVs;

				fixed4 texColor = tex2D(_MainTex, scrollUVs);
				fixed4 addColor = tex2D(_AddTex, scrollUVs);

				float3 worldNormal = normalize(i.worldNormal);
				float3 viewDir = normalize(i.viewDir);

				// Scanlines
				float scan = 0.0;
				scan += _UseScan * ApplyScan(i.uv.z, _ScanTiling, _ScanSpeed, _Time.w);

				// Glow
				float glow = 0.0;
				glow += _UseGlow * frac(i.worldVertex.y * _GlowTiling - _Time.x * _GlowSpeed);

				// Flicker
				fixed4 flicker = tex2D(_FlickerTex, _Time * _FlickerSpeed);

				// Rim Light
				half rim = 1.0-saturate(dot(viewDir, worldNormal));
				fixed4 rimColor = _RimColor * pow (rim, _RimPower);

				fixed4 col = texColor * _MainColor + (glow * 0.35 * _MainColor) + rimColor;
				col.a = texColor.a * _Alpha * (scan + rim + glow) * flicker;

				col.rgb += addColor * _AddColor;
				col.a = max(col.a, addColor.w*dot(addColor.xyz, 0.333));

				col.rgb *= _Brightness;

				col *= i.color.a;

				return col;
			}
			ENDCG
		}
	}

	//CustomEditor "HologramShaderGUI"
}
